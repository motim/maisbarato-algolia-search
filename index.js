const algoliasearch = require('algoliasearch');
const dotenv = require('dotenv');
const admin = require("firebase-admin");
const serviceAccount = require('./supermarkets-products-index-firebase-adminsdk-jnupe-800b1722ce.json');
const express = require('express');

// load values from the .env file in this directory into process.env
dotenv.load();

// configure firebase
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASE_DATABASE_URL,
});
const fb = admin.firestore();
const collectionName = 'products';

// configure algolia
const algolia = algoliasearch(
    process.env.ALGOLIA_APP_ID,
    process.env.ALGOLIA_API_KEY
);
const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);

// watch 'products' collectioon from fb and insert or update into mongo
const query = fb.collection(collectionName);
const observer = query.onSnapshot(
    querySnapshot => {
        const addOrUpdateRecords = [];
        const deleteRecords = [];
        querySnapshot.docChanges.forEach(doc => {
            if (doc.type === 'removed') {
                deleteRecords.push(
                    doc.doc.id
                )
            }else {
                const data = doc.doc.data();
                data['objectID'] = doc.doc.id;
                addOrUpdateRecords.push(data);
            }
        });

        index
            .saveObjects(addOrUpdateRecords)
            .then(() => {
                console.log('Products added or updated into Algolia');
            })
            .catch(error => {
                console.error(
                    'Error when importing products into Algolia',
                    error
                );
            });

        if (deleteRecords.length > 0){
            index.deleteObjects(deleteRecords, (err, content) => {
                console.log('Deleting records: ', err, content);
            });
        }
    },
    err => {
        console.log(`Encountered error: ${err}`);
    }
);

const app = express();

app.get('/', (req, res) => {
    res.send('Algolia is alive');
});

app.listen(5002);